function createCard(name, description, pictureUrl) {
    return `
      <div class="card shadow mb-5 bg-white rounded" style="height: min-content;">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="card-text">${description}</p>
        </div>


      </div>
    `;
}



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        throw new Error("Response not ok");
      } else {
        const data = await response.json();
        
        //const conference = data.conferences[0];
        //const nameTag = document.querySelector('.card-title');
        //nameTag.innerHTML = conference.name;

        for (let conference of data.conferences){
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const html = createCard(title, description, pictureUrl);
                console.log(html);
                
                const column = document.querySelector('.col');
                column.innerHTML += html;     

                // console.log(details); // (note to self) console.log is the same as print (open your console with inpsect and check it out)
                    //-const detailTag = document.querySelector('.card-text');
                    //-detailTag.innerHTML = details.conference.description;

                    // (note to self) set a const = to document.querySelector("what goes in here is found in your index.html")  
                    // ^ thats the basic format of what we're doing here 
                    // above these notes is the conference details, directly below is the image that appears in the little card.

                    //-const imgTag = document.querySelector('.card-img-top');
                    //-imgTag.src = details.conference.location.picture_url;
            }
        }

      }
    } catch (e) {
        console.error(e)
        const alertMsg = createAlertMessage(e.message);
        const row = document.querySelector('.row');
        row.innerHTML += alertMsg; 

    }
  
  });